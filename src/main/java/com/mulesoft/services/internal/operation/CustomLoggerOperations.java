package com.mulesoft.services.internal.operation;

import static org.mule.runtime.extension.api.annotation.param.MediaType.ANY;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;

import com.google.gson.JsonObject;
import com.mulesoft.services.Constants;
import com.mulesoft.services.internal.metadata.DefaultLogDetails;
import com.mulesoft.services.internal.metadata.DefaultLogSettings;
import com.mulesoft.services.internal.metadata.ExceptionDetails;
import com.mulesoft.services.internal.metadata.LoggerUtils;


/**
 * This class is a container for operations, every public method in this class will be taken as an extension operation.
 * Author: Ashfaq Framewala
 */
public class CustomLoggerOperations {

	protected transient Log logger;
	
	private static String HOSTNAME;

	static {
		// this is not necessarily perfect, but should be good enough. see
		// http://stackoverflow.com/a/7800008 for details
		if (System.getenv("COMPUTERNAME") != null) {
			// Windows environment variable
			HOSTNAME = System.getenv("COMPUTERNAME");
		} else if (System.getenv("HOSTNAME") != null) {
			// some Linux systems (and maybe UNIX systems) have this
			HOSTNAME = System.getenv("HOSTNAME");
		} else {
			// use the name of "localhost"
			try {
				HOSTNAME = Inet4Address.getLocalHost().getHostName();
			} catch (UnknownHostException e) {
				HOSTNAME = "[unknown]";
			}
		}
	}

	/**
	 * This method should be  used in an Error Handler to log additional information about the error
	 * @param settings
	 * @return void
	 */
	/* Author: Ashfaq Framewala
	 * Name of the function is set as logCustomException as logException casues an error in Anypoint Studio
	 */
  @MediaType(value = ANY, strict = false)
  public void logCustomException(@ParameterGroup(name="Settings") DefaultLogSettings settings,
		  @ParameterGroup(name="Exception Details") ExceptionDetails details) {
	  
	initLogger(details.getLogCategory().category());
	  
	String strErrorMessage = details.getExceptionMsg() != null? details.getExceptionMsg() : "" ;

	String strErrDetailMessage = details.getExceptionDetailMsg() != null? details.getExceptionDetailMsg() : "";
	  
	JsonObject logSettings =  getLogMsgAsJSON(settings);

	JsonObject exceptionDetails = new JsonObject();
	exceptionDetails.addProperty("errorMessage", strErrorMessage);
	exceptionDetails.addProperty("errorDetailedMessage", strErrDetailMessage);
	exceptionDetails.addProperty("severity",details.getExceptionSeverity().severity());
	exceptionDetails.addProperty("classification",details.getExceptionType().type());
	
	
	logSettings.add("exceptionDetails", exceptionDetails);

	logWithLevel(logSettings.toString(), settings.getLogLevel().logLevel());
  }
  
	/**
	 * This method should be  used to log messages to the logger - preferably for the INFO and DEBUG ones
	 * @param settings
	 * @return void
	 */
	/* Author: Ashfaq Framewala
	 * Name of the function is set as logCustomException as logException casues an error in Anypoint Studio
	 */
@MediaType(value = ANY, strict = false)
public void logDefault(@ParameterGroup(name="Settings") DefaultLogSettings settings,
		  @ParameterGroup(name="Details") DefaultLogDetails details) {
	  initLogger(details.getLogCategory().category());
	JsonObject jsonObject = getLogMsgAsJSON(settings);

	logWithLevel(jsonObject.toString(), settings.getLogLevel().logLevel());
}

	private JsonObject getLogMsgAsJSON(DefaultLogSettings settings) {
		
		String strExpressionResult = settings.getStrExpression();
		if(strExpressionResult.equals(Constants.DEFAULT_EXPRESSION_FIELD_VALUE)) {
			strExpressionResult = "";
		}
		  
		String env = settings.getEnvAsString();
		String appVersion = settings.getAppVersionAsString();
		String strCorrelationId = settings.getCorrelationIdAsString();
		String appName = settings.getAppNameAsString();
		String tracePoint = settings.getTracePoint() != null? settings.getTracePoint() : "";
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("hostname", HOSTNAME);
		jsonObject.addProperty("expression", strExpressionResult);
		jsonObject.addProperty("correlationId", strCorrelationId);
		jsonObject.addProperty("logMessage", settings.getLogMessage());
		jsonObject.addProperty("appName", appName);
		jsonObject.addProperty("appVersion", appVersion);
		jsonObject.addProperty("tracePoint", tracePoint);
		jsonObject.addProperty("env", env);
		jsonObject.addProperty("flowName", settings.getFlowName());
		return jsonObject;
	}
  
  protected void initLogger(String category) {
		this.logger = LogFactory.getLog(category);
	}
  
  
  protected void logWithLevel(String logMessage, String logLevel) {
	  
	  if (LoggerUtils.LogLevel.ERROR.logLevel().equals(logLevel)) {
		  logger.error(logMessage);
	  }
	  else if (LoggerUtils.LogLevel.WARN.logLevel().equals(logLevel)) {
		  logger.warn(logMessage);
	  }
	  else if (LoggerUtils.LogLevel.DEBUG.logLevel().equals(logLevel)) {
		  logger.debug(logMessage);
	  }
	  else if (LoggerUtils.LogLevel.TRACE.logLevel().equals(logLevel)) {
		  logger.trace(logMessage);
	  }
	  else {
		  logger.info(logMessage);
	  }
	  
  }
  
}
