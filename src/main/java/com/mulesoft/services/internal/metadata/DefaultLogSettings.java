package com.mulesoft.services.internal.metadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;

import com.mulesoft.services.Constants;


public class DefaultLogSettings {
	@Parameter
	@DisplayName("Log Message")
	@Summary("Message to be logged")
	String  logMessage;

	
	public String getLogMessage() {
		return logMessage;
	}

	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}
	
	@Parameter
	@DisplayName("Flow Name")
	@Summary("Flow Name")
	String  flowName;
	
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	
	public String getFlowName() {
		return flowName;
	}
	
	@Parameter
	@DisplayName("Trace Point")
	@Summary("Trace Point")
	@Example("START_OF_FLOW")
	@Optional
	String  tracePoint;
	
	public String getTracePoint() {
		return tracePoint;
	}

	public void setTracePoint(String tracePoint) {
		this.tracePoint = tracePoint;
	}

	@Parameter
	@DisplayName("Log Expression")
	@Summary("Use this parameter if a dw expression has to be logged")
	@Optional(defaultValue="#[\"\"]")
	@Example("#[payload]")
	ParameterResolver<InputStream>  expr;

	public ParameterResolver<InputStream> getExpr() {
		return expr;
	}
	
	public String getStrExpression() {
		return resolveExpression(expr);	
	}
	
	public void setExpr(ParameterResolver<InputStream> expr) {
		this.expr = expr;
	}
	
	@Parameter
	@DisplayName("Application Name")
	@Summary("Application Name")
	@Optional(defaultValue="#[mule.muleContext.config.id]")
	ParameterResolver<InputStream> appName;

	public ParameterResolver<InputStream> getAppName() {
		return appName;
	}

	public void setAppName(ParameterResolver<InputStream> appName) {
		this.appName = appName;
	}
	
	public String getAppNameAsString() {
		return resolveExpression(appName);	
	}
	
	@Parameter
	@DisplayName("Application Version")
	@Summary("Application Version")
	@Optional(defaultValue="#[p(\"app.version\")]")
	ParameterResolver<InputStream> appVersion;
	
	public ParameterResolver<InputStream> getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(ParameterResolver<InputStream> appVersion) {
		this.appVersion = appVersion;
	}
	
	public String getAppVersionAsString() {
		return resolveExpression(appVersion);	
	}	
	
	@Parameter
	@DisplayName("Environment")
	@Summary("Application Version")
	@Optional(defaultValue="#[p(\"env\")]")
	ParameterResolver<InputStream> env;
	
	public ParameterResolver<InputStream> getEnv() {
		return env;
	}

	public void setEnv(ParameterResolver<InputStream> env) {
		this.env = env;
	}
	
	public String getEnvAsString() {
		return resolveExpression(env);	
	}

	@Parameter
	@DisplayName("Correlation ID")
	@Summary("Correlation ID to be set")
	@Optional(defaultValue="#[if(attributes.headers.correlationId?) attributes.headers.correlationId else correlationId]")
	ParameterResolver<InputStream> correlationId;

	public ParameterResolver<InputStream> getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(ParameterResolver<InputStream> correlationId) {
		this.correlationId = correlationId;
	}
	
	public String getCorrelationIdAsString() {
		return resolveExpression(correlationId);	
	}

	private String resolveExpression(ParameterResolver<InputStream> muleExpression) {
		String result = Constants.DEFAULT_EXPRESSION_FIELD_VALUE;
		try {
			result =  expressionAsString(muleExpression);
		} 
		catch (Exception e) {
			//
		}
		return result;
	}

	private String expressionAsString(ParameterResolver<InputStream> muleExpression) throws IOException {
		String result = null;
		if (muleExpression == null) return result;
		if (muleExpression.getExpression() == null || muleExpression.getExpression().get().trim().equals("") 
				|| muleExpression.getExpression().get().equals("#[]")) {
			return result;
		}
		
		InputStream stream = muleExpression.resolve();
		
		  
		StringBuilder textBuilder = new StringBuilder();
		Reader reader = new BufferedReader(new InputStreamReader(stream, Charset.forName(StandardCharsets.UTF_8.name())));
		 int c = 0;
		 while ((c = reader.read()) != -1) {
		    textBuilder.append((char) c);
		 }
		 
		 result = textBuilder.toString();
		 return result;
	}
	
	/*
	@Parameter
	@DisplayName("Log Level")
	@Optional(defaultValue="ERROR")
	LogLevel level;
	
	public void setLevel(LogLevel level) {
		this.level = level;
	}

	public LogLevel getLevel() {
		return level;
	}*/
	
	@Parameter
	@DisplayName("Log Level")
	@Optional(defaultValue="INFO")
	private LoggerUtils.LogLevel logLevel;
	
	
	public LoggerUtils.LogLevel getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(LoggerUtils.LogLevel logLevel) {
		this.logLevel = logLevel;
	}


}
